<?php

Route::pattern('month', '[0-9]+');

Route::group(['prefix' => 'v1', 'namespace' => 'API\V1'], function () {


    Route::get('/', ['as' => 'api.base', function(){
        return '';
    }]);


    Route::resource('company', 'CompanyController', ['except' => ['create', 'edit']]);
    Route::get('company-blocked/{month}', ['as' => 'company.blocked', 'uses' => 'CompanyController@blocked']);

    Route::resource('user', 'UserController', ['except' => ['create', 'edit']]);
    Route::get('abuser/{month}', ['as' => 'user.abuser', 'uses' => 'UserController@abuser']);

    Route::resource('transfer', 'TransferController', ['only' => ['index', 'show']]);
    Route::post('transfer-generate', ['as' => 'transfer.generate', 'uses' => 'TransferController@generate']);
});
