@extends('layouts.default')

@section('content')

    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">{{trans('web.toggle_navigation')}}</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#home">{{$appName}}</a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#companies">{{trans('web.companies')}}</a></li>
                    <li><a href="#users">{{trans('web.users')}}</a></li>
                    <li><a href="#companies-blocked">{{trans('web.blocked_companies')}}</a></li>
                    <li><a href="#abusers">{{trans('web.abusers')}}</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>

        {{-- Progress bar --}}
        <div id="progressBar" class="progress invisible">
            <div class="progress-bar progress-bar-striped active" role="progressbar">
            </div>
        </div>

    </nav>

    {{-- Alert block --}}
    <div class="container">
        <div id="alertContainer" class="alert alert-success alert-dismissable collapse">
            <a href="#" class="close" aria-label="close">&times;</a>
            <span class="v-message"></span>
        </div>
    </div>
    {{-- Container --}}

    <div class="container" id="page-container"></div>

    {{-- Templates --}}

    <script id="home-page-template" type="text/x-handlebars-template">
        <div id="home-page" class="page active">
            <div class="row">
                <h3>{{trans('web.toggle_navigation')}}</h3>
            </div>
        </div>
    </script>

    <script id="companies-page-template" type="text/x-handlebars-template">
        <div id="companies-page" class="page">
            <div class="row">
                <h3>{{trans('web.companies')}}</h3>
            </div>
            <div class="row">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{trans('web.name')}}</th>
                        <th>{{trans('web.quota')}}</th>
                        <th class="text-right width-200 padding-r-30">{{trans('web.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @{{#each company}}
                    <tr class="v-item">
                        <td class="v-id">@{{id}}</td>
                        <td class="v-name">@{{name}}</td>
                        <td class="v-quota">@{{quota}}</td>
                        <td class="hidden v-quota-abs">@{{quota_abs}}</td>
                        <td class="text-right width-200">
                            <a class="btn btn-sm btn-danger v-delete" href="#">{{trans('web.delete')}}</a>
                            <a class="btn btn-sm btn-default v-edit" href="#">{{trans('web.edit')}}</a>
                        </td>
                    </tr>
                    @{{/each}}
                    </tbody>
                </table>
                <a id="cmdAddCompany" class="btn btn-sm btn-success" href="#">{{trans('web.add')}}</a>
            </div>
        </div>
    </script>

    <script id="users-page-template" type="text/x-handlebars-template">
        <div id="users-page" class="page">
            <div class="row">
                <h3>{{trans('web.users')}}</h3>
            </div>
            <div class="row">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{trans('web.name')}}</th>
                        <th>{{trans('web.email')}}</th>
                        <th>{{trans('web.company')}}</th>
                        <th class="text-right width-200 padding-r-30">{{trans('web.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @{{#each user}}
                    <tr class="v-item">
                        <td class="v-id">@{{id}}</td>
                        <td class="v-name">@{{name}}</td>
                        <td class="v-email">@{{email}}</td>
                        <td>@{{company}}</td>
                        <td class="hidden v-company-id">@{{company_id}}</td>
                        <td class="text-right width-200">
                            <a class="btn btn-sm btn-danger v-delete" href="#">{{trans('web.delete')}}</a>
                            <a class="btn btn-sm btn-default v-edit" href="#">{{trans('web.edit')}}</a>
                        </td>
                    </tr>
                    @{{/each}}
                    </tbody>
                </table>
                <a id="cmdAddUser" class="btn btn-sm btn-success" href="#">{{trans('web.add')}}</a>
            </div>
        </div>
    </script>

    <script id="companies-blocked-page-template" type="text/x-handlebars-template">
        <div id="companies-blocked-page" class="page">
            <div class="row">
                <h3>{{trans('web.blocked_companies')}}</h3>
            </div>
            <div class="row">
                <div class="col-md-2 pull-right">
                    <select class="form-control max-width-200" id="companyMonth" name="companyMonth">
                        @foreach ($months as $k => $month)
                            <option value="{{++$k}}">{{ $month->format('F Y')}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row margin-t-10">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{trans('web.name')}}</th>
                        <th>{{trans('web.quota')}}</th>
                        <th>{{trans('web.used')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @{{#each company}}
                    <tr>
                        <td>@{{id}}</td>
                        <td>@{{name}}</td>
                        <td>@{{quota}}</td>
                        <td>@{{bytes}}</td>
                    </tr>
                    @{{/each}}
                    </tbody>
                </table>
            </div>
        </div>
    </script>

    <script id="abusers-page-template" type="text/x-handlebars-template">
        <div id="abusers-page" class="page">
            <div class="row">
                <h3>{{trans('web.abusers')}}</h3>
            </div>
            <div class="row">
                <div class="col-md-offset-6 col-md-6 text-right">
                    <select class="form-control max-width-200 margin-r-30 inline-block" id="abuserMonth" name="abuserMonth">
                        @foreach ($months as $k => $month)
                            <option value="{{++$k}}">{{ $month->format('F Y')}}</option>
                        @endforeach
                    </select>
                    <a id="cmdGenerateData" class="btn btn-success min-width-110 margin-r-30" href="#">{{trans('web.generate_data')}}</a>
                    <a id="cmdShowReport" class="btn btn-info min-width-110" href="#">{{trans('web.show_report')}}</a>
                </div>
            </div>
            <div class="row margin-t-10">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{trans('web.name')}}</th>
                        <th>{{trans('web.email')}}</th>
                        <th>{{trans('web.company')}}</th>
                        <th>{{trans('web.used')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @{{#each user}}
                    <tr>
                        <td>@{{id}}</td>
                        <td>@{{name}}</td>
                        <td>@{{email}}</td>
                        <td>@{{company}}</td>
                        <td>@{{bytes}}</td>
                    </tr>
                    @{{/each}}
                    </tbody>
                </table>
            </div>
        </div>
    </script>

    <script id="error-page-template" type="text/x-handlebars-template">
        <div id="error-page" class="page">
            <h3>{{trans('web.404_text')}}</h3>
        </div>
    </script>

    <!-- Modal -->
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="dlgModal" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">{{trans('web.close')}}</span>
                    </button>
                    <h4 class="modal-title" id="title">
                        <span class="v-type-company">{{trans('web.company')}}</span>
                        <span class="v-type-user">{{trans('web.user')}}</span>
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <form class="form">
                        <input id="type" type="hidden">
                        <input id="id" type="hidden">
                        <div class="form-group">
                            <label for="name">{{trans('web.name')}}</label>
                            <input id="name" name="name" type="text" class="form-control"/>
                        </div>
                        <div class="form-group v-type-user">
                            <label for="email">{{trans('web.email')}}</label>
                            <input id="email" name="email" type="email" class="form-control"/>
                        </div>
                        <div class="form-group v-type-company">
                            <label for="quota">{{trans('web.quota')}} (B)</label>
                            <input id="quota" name="quota" type="number" class="form-control"/>
                        </div>
                        <div class="form-group v-type-user">
                            <label for="company">{{trans('web.company')}}</label>
                            <select class="form-control" id="company" name="company"></select>
                        </div>
                    </form>
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('web.cancel')}}</button>
                    <button id="cmdDlgSave" type="button" class="btn btn-primary">{{trans('web.save')}}</button>
                </div>
            </div>
        </div>
    </div>

@stop

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">
        const LANG = {
            saved_successfully: '{{trans('web.saved_successfully')}}',
            deleted_successfully: '{{trans('web.deleted_successfully')}}',
            error_unknown: '{{trans('web.error_unknown')}}',
            error_connection: '{{trans('web.error_connection')}}',
            generated_successfully: '{{trans('web.generated_successfully')}}'
        }
    </script>
@stop