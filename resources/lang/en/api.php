<?php

return [

    'success' => 'Success',
    "error_not_found" => "Not found",
    "error_company_not_found" => "The company is not found",
    "error_user_not_found" => "The user is not found",
    "error_transfer_not_found" => "The transfer is not found",
    'error_inoperable_entity' => 'Inoperable entity',
    'error_bad_request' => 'Bad request',
    'deleted_success' => 'Deleted successfully',
    'error_internal_error' => 'Internal Error',
    'error_month_number' => 'The month number',
    'month' => 'month',

];
