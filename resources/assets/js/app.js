require('./bootstrap');

$(document).ready(function () {
    spaRouter.init();
    window.location.hash = "#home";
});


/**
 * Location polyfill for ie, ff < 21.0 and safari
 */
if (typeof window.location.origin === "undefined") {
    window.location.origin = window.location.protocol + "//" + window.location.host;
}

/**
 * Page container selector
 */
window.pageContainer = $("#page-container");

/**
 * Modal
 */
window.modalEdit = $('#modalEdit');

/**
 * Alert
 */
window.alertContainer = $('#alertContainer');

/**
 * Cached companies
 * @type {{}}
 */
window.companies = {};

/**
 * Helper
 *
 * @type {{renderPageTemplate: utils.renderPageTemplate, pageNotFoundError: utils.pageNotFoundError, fetch: utils.fetch}}
 */
var utils = {
    /**
     * Render template
     *
     * @param templateId
     * @param data
     */
    renderPageTemplate: function (templateId, data) {
        var _data = data || {};
        var templateScript = $(templateId).html();
        var template = handlebars.compile(templateScript);
        window.pageContainer.empty();
        window.pageContainer.append(template(_data));
    },
    /**
     * 404
     */
    pageNotFoundError: function () {
        this.renderPageTemplate("#error-page-template");
    },
    /**
     * Set company name
     *
     * @param users
     * @param companies
     * @returns {*}
     */
    retrieveCompanyName: function (users, companies) {
        _.forOwn(users, function (user, key) {
            var company = companies.filter(function (el) {
                return el.id === user.company_id;
            })[0];
            user.company = ('undefined' !== typeof company) ? company.name : '';
        });
        return {user: users};
    },
    /**
     * Refill modal select edit user
     */
    populateCompanySelectBox: function () {
        var selectBox = window.modalEdit.find('#company');
        selectBox.empty();
        _.forOwn(window.companies.company, function (company, key) {
            selectBox.append('<option value="' + company.id + '">' + company.name + '</option>');
        });
    },
    getValidationRules: function (entityType) {
        var rules = {
            name: {
                required: true,
                minlength: 1,
                maxlength: 150
            }
        };
        switch (entityType) {
            case 'company': {
                rules.quota = {
                    maxlength: 15,
                    digits: true
                }
            }
                break;
            case 'user': {
                rules.email = {
                    required: true,
                    minlength: 3,
                    maxlength: 150,
                    email: true
                };
                rules.company = {
                    required: true
                }
            }
                break;
        }
        return rules
    },
    /**
     * Fetch data
     *
     * @param method
     * @param url
     * @param data
     * @param onSuccess
     */
    call: function (method, url, data, onSuccess) {
        var _that = this;
        var _data = data || {};
        $('#progressBar').removeClass('invisible');
        axios({
            method: method,
            url: API_BASE_URL + url,
            data: _data
        }).then(function (response) {
            $('#progressBar').addClass('invisible');
            if (typeof onSuccess === 'function') {
                onSuccess(response);
            } else {
                console.log(response);
            }
        })
            .catch(function (error) {
                $('#progressBar').addClass('invisible');
                if (error.response) {
                    var message = LANG.error_unknown;
                    if ('undefined' !== typeof error.response.data.error) {
                        message = error.response.data.error.message + '<ul>';
                        _.forOwn(error.response.data.error.data, function (errorMess, key) {
                            message += '<li>' + errorMess + '</li>';
                        });
                        message += '</ul>';
                    }
                    _that.showAlertMessage('danger', message);
                } else if (error.request) {
                    _that.showAlertMessage('danger', LANG.error_connection);
                } else {
                    _that.showAlertMessage('danger', LANG.error_unknown);
                }
            });
    },
    fetchCompanies: function (force) {
        force = typeof force !== 'undefined' ? force : false;
        if (force || _.isEmpty(window.companies)) {
            var _that = this;
            this.call('GET', 'company', {}, function (response) {
                window.companies = response.data.success.data;
                _that.renderPageTemplate("#companies-page-template", window.companies);
                _that.populateCompanySelectBox();
            })
        } else {
            this.renderPageTemplate("#companies-page-template", window.companies);
        }
    },
    fetchCompaniesBlocked: function (id) {
        var _that = this;
        this.call('GET', 'company-blocked/' + id, {}, function (response) {
            _that.renderPageTemplate("#companies-blocked-page-template", response.data.success.data);
            $('#companyMonth').val(id);
        })
    },
    fetchUsers: function () {
        var _that = this;
        var fnUsers = function () {

            /**
             *  Fetch users only if the companies exist
             */
            if (!_.isEmpty(window.companies)) {
                _that.call('GET', 'user', {}, function (response) {
                    var users = _that.retrieveCompanyName(response.data.success.data.user, window.companies.company);
                    _that.renderPageTemplate("#users-page-template", users);
                })
            }
        };

        /**
         * Try to fetch the companies
         */
        if (_.isEmpty(window.companies)) {
            this.call('GET', 'company', {}, function (response) {
                window.companies = response.data.success.data;
                _that.populateCompanySelectBox();
                fnUsers();
            })
        } else {
            fnUsers();
        }
    },
    fetchAbusers: function (id) {
        var _that = this;
        var fnUsers = function () {

            /**
             *  Fetch abusers only if the companies exist
             */
            if (!_.isEmpty(window.companies)) {
                _that.call('GET', 'abuser/' + id, {}, function (response) {
                    var users = _that.retrieveCompanyName(response.data.success.data.user, window.companies.company);
                    _that.renderPageTemplate("#abusers-page-template", users);
                    $('#abuserMonth').val(id);
                })
            }
        };

        /**
         * Try to fetch the companies
         */
        if (_.isEmpty(window.companies)) {
            this.call('GET', 'company', {}, function (response) {
                window.companies = response.data.success.data;
                fnUsers();
            })
        } else {
            fnUsers();
        }
    },
    saveCompany: function (data) {
        var _that = this;
        var method = data.id ? 'PUT' : 'POST';
        this.call(method, 'company' + (data.id ? '/' + data.id : ''), data, function (response) {
            _that.fetchCompanies(true);
            _that.showAlertMessage('success', LANG.saved_successfully);
        })
    },
    saveUser: function (data) {
        var _that = this;
        var method = data.id ? 'PUT' : 'POST';
        this.call(method, 'user' + (data.id ? '/' + data.id : ''), data, function (response) {
            _that.fetchUsers();
            _that.showAlertMessage('success', LANG.saved_successfully);
        })
    },
    deleteCompany: function (id) {
        var _that = this;
        this.call('DELETE', 'company/' + id, {}, function (response) {
            _that.fetchCompanies(true);
            _that.showAlertMessage('success', LANG.deleted_successfully);
        })
    },
    deleteUser: function (id) {
        var _that = this;
        this.call('DELETE', 'user/' + id, {}, function (response) {
            _that.fetchUsers();
            _that.showAlertMessage('success', LANG.deleted_successfully);
        })
    },
    generateData: function () {
        var _that = this;
        this.call('POST', 'transfer-generate', {}, function (response) {
            _that.showAlertMessage('success', LANG.generated_successfully);
        })
    },
    showModal: function (data) {
        var _that = this;
        var entityType = 'undefined' !== typeof data.user ? 'user' : 'company';
        /**
         * Common fields
         */
        window.modalEdit.find('#type').val(entityType);
        window.modalEdit.find('#id').val(data[entityType].id);
        window.modalEdit.find('#name').val(data[entityType].name);
        /**
         * Special fields
         */
        switch (entityType) {
            case 'company':
                window.modalEdit.find('#quota').val(data.company.quota);
                break;
            case 'user':
                window.modalEdit.find('#email').val(data.user.email);
                window.modalEdit.find('#company').val(data.user.company_id);
                break;
        }

        /**
         * Switch visibility
         */
        window.modalEdit.find('[class*=v-type-]').hide();
        window.modalEdit.find('.v-type-' + entityType).show();

        /**
         * Validation
         */
        var form = window.modalEdit.find('.form');
        form.removeData('validator');
        form.validate({
            rules: this.getValidationRules(entityType)
        }).resetForm();

        /**
         *  On save click
         */
        window.modalEdit.find('#cmdDlgSave').on('click', function () {
            if (form.valid()) {
                var entityType = window.modalEdit.find('#type').val();
                var data = {
                    id: window.modalEdit.find('#id').val(),
                    name: window.modalEdit.find('#name').val()
                };
                switch (entityType) {
                    case 'company':
                        data.quota = window.modalEdit.find('#quota').val();
                        _that.saveCompany(data);
                        break;
                    case 'user':
                        data.email = window.modalEdit.find('#email').val();
                        data.company_id = window.modalEdit.find('#company').val();
                        _that.saveUser(data);
                        break;
                }
                window.modalEdit.find('#cmdDlgSave').unbind("click");
                window.modalEdit.modal('hide');
            }
        });
        window.modalEdit.modal('show');
    },
    showAlertMessage: function (type, message) {
        window.alertContainer.removeClass('alert-success')
            .removeClass('alert-danger')
            .removeClass('alert-warning')
            .removeClass('alert-info')
            .addClass('alert-' + type);
        window.alertContainer.find('.v-message').html(message);
        window.alertContainer.show();
    }

};

/**
 *  Router - Handles routing and rendering for the pages
 *
 *  Summary:
 *      - url hash changes
 *      - render function checks routes for the hash changes
 *      - function for that hash gets called and loads page content
 */
var Router = {
    routes: {},
    init: function () {
        this.bindEvents();
        $(window).trigger("hashchange");
    },
    bindEvents: function () {
        $(window).on("hashchange", this.render.bind(this));
    },
    render: function () {
        var keyName = window.location.hash.split("/")[0];
        var url = window.location.hash;
        window.pageContainer.find(".active").hide().removeClass("active");
        if (this.routes[keyName]) {
            this.routes[keyName](url);
        } else {
            utils.pageNotFoundError();
        }
    }
};

/**
 * Routes
 *
 * @type {{#home: spaRoutes.#home, #companies: spaRoutes.#about, #users: spaRoutes.#users}}
 */
var spaRoutes = {
    "#home": function () {
        utils.renderPageTemplate("#home-page-template");
    },
    "#companies": function () {
        utils.fetchCompanies();
    },
    "#users": function () {
        utils.fetchUsers();
    },
    "#companies-blocked": function () {
        utils.fetchCompaniesBlocked(1);
    },
    "#abusers": function () {
        utils.fetchAbusers(1);
    }
};

var spaRouter = $.extend({}, Router, {
    routes: spaRoutes
});

/**
 * Close alert
 */
window.alertContainer.find('.close').on('click', function (e) {
    e.preventDefault();
    $(this).parent().hide();
});

/**
 * Add company
 */
$(document).on('click', '#cmdAddCompany', function (e) {
    e.preventDefault();
    var company = {
        id: '',
        name: '',
        quota: ''
    };
    utils.showModal({company: company});
});

/**
 * Edit company
 */
$(document).on('click', '#companies-page .v-edit', function (e) {
    e.preventDefault();
    var elContainer = $(this).closest('.v-item');
    var company = {
        id: elContainer.find('.v-id').text(),
        name: elContainer.find('.v-name').text(),
        quota: elContainer.find('.v-quota-abs').text()
    };
    utils.showModal({company: company});
});

/**
 * Delete company
 */
$(document).on('click', '#companies-page .v-delete', function (e) {
    e.preventDefault();
    var elContainer = $(this).closest('.v-item');
    var id = elContainer.find('.v-id').text();
    utils.deleteCompany(id);
});

/**
 * Add user
 */
$(document).on('click', '#cmdAddUser', function (e) {
    e.preventDefault();
    var user = {
        id: '',
        name: '',
        email: '',
        company_id: ''
    };
    utils.showModal({user: user});
});

/**
 * Edit user
 */
$(document).on('click', '#users-page .v-edit', function (e) {
    e.preventDefault();
    var elContainer = $(this).closest('.v-item');
    var user = {
        id: elContainer.find('.v-id').text(),
        name: elContainer.find('.v-name').text(),
        email: elContainer.find('.v-email').text(),
        company_id: elContainer.find('.v-company-id').text()
    };
    utils.showModal({user: user});
});

/**
 * Delete user
 */
$(document).on('click', '#users-page .v-delete', function (e) {
    e.preventDefault();
    var elContainer = $(this).closest('.v-item');
    var id = elContainer.find('.v-id').text();
    utils.deleteUser(id);
});

/**
 * On change company month
 */
$(document).on('change', '#companyMonth', function (e) {
    var id = $(this).val();
    utils.fetchCompaniesBlocked(id);
});

/**
 * On click generate data
 */
$(document).on('click', '#cmdGenerateData', function (e) {
    e.preventDefault();
    utils.generateData();
});

/**
 * On click show report
 */
$(document).on('click', '#cmdShowReport', function (e) {
    e.preventDefault();
    var id = $('#abuserMonth').val();
    utils.fetchAbusers(id);
});
