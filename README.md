
## Installation


- Clone the project
- Create a database
- Rename *.env.example* into *.env* and set the database options
- Run: *composer install*
- Run *php artisan key:generate*
- Run migrations: *php artisan migrate*
- Run seeder to populate companies and users: *php artisan db:seed*
