<?php namespace App\Exceptions\API;

use Illuminate\Validation\ValidationException;

class ApiModelValidationException extends ValidationException
{

    /**
     * @var ValidationException
     */
    private $validationException;

    public function __construct(ValidationException $validationException)
    {
        $this->validationException = $validationException;
        parent::__construct($validationException->validator, $validationException->response);
    }

    public function getErrors()
    {
        return $this->validationException->validator->errors()->getMessages();
    }
}