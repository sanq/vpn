<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class HomeController extends Controller
{

    /**
     * Index view
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $appName = config('app.name');
        $configMonth = config('my.report_last_months');
        $months = Collection::make(null);
        $starFrom = Carbon::now()->endOfMonth()->addMinute()->subMonths($configMonth);
        while ($starFrom < Carbon::now()) {
            $months->prepend($starFrom->copy());
            $starFrom->addMonth();
        }
        return view('index', compact('appName', 'months'));
    }

}
