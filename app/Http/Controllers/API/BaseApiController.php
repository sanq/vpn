<?php

namespace App\Http\Controllers\API;

use App\Exceptions\API\ApiModelValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use \Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;
use Fractal;

class BaseApiController extends Controller
{

    protected $statusCode = 200;

    /**
     * Fractal manager instance
     *
     * @var \League\Fractal\Manager
     */
    protected $manager;

    function __construct()
    {
        $this->manager = Fractal::getManager();
    }

    /**
     * Successful response
     *
     * @param $message
     * @param array $data
     * @return JsonResponse
     */
    private function respondWithSuccess($message, $data = [], $code = 200)
    {
        return $this->setStatusCode($code)->respond([
            'success' => [
                'message' => $message,
                'data' => $data
            ]
        ]);
    }

    /**
     * Unsuccessful response
     *
     * @param string $message
     * @param array $errors
     * @param int $code
     * @return JsonResponse
     */
    private function respondWithError($message, $errors = [], $code = 400)
    {
        return $this->setStatusCode($code)->respond([
            'error' => [
                'message' => $message,
                'data' => $errors
            ]
        ]);
    }

    /**
     * Helper respond
     *
     * @param $data
     * @param array $headers
     * @return JsonResponse
     */
    private function respond($data, $headers = [])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    /**
     * Status code getter
     *
     * @return int
     */
    private function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Status code setter
     *
     * @param $statusCode
     * @return $this
     */
    private function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * Get transformed single-item
     *
     * @param Model $model
     * @param TransformerAbstract $transformer
     * @return array
     */
    protected function createItemData(Model $model, TransformerAbstract $transformer)
    {
        $className = strtolower(class_basename($model));
        return [$className => $this->manager->createData(new Item($model, $transformer))->toArray()['data']];
    }

    /**
     * Get transformed collection
     *
     * @param Collection $collection
     * @param TransformerAbstract $transformer
     * @return array
     */
    protected function createCollectionData(Collection $collection, TransformerAbstract $transformer)
    {
        $model = $collection->first();
        $className = strtolower(class_basename($model));
        return [$className => $this->manager->createData(new FractalCollection($collection, $transformer))->toArray()['data']];
    }

    /**
     * Response on inoperable entity
     *
     * @param string $message
     * @return JsonResponse
     */
    public function respondInternalError($message = '')
    {
        $message = (isset($message) && $message) ? $message : trans('api.error_internal_error');
        return $this->respondWithError($message, [], 500);
    }

    /**
     * Response on inoperable entity
     *
     * @param string $message
     * @param array $validationErrors
     * @return JsonResponse
     */
    public function respondInoperableEntity($message = '', $validationErrors = [])
    {
        $message = (isset($message) && $message) ? $message : trans('api.error_inoperable_entity');
        return $this->respondWithError($message, $validationErrors, 422);
    }

    /**
     * Response on bad request
     *
     * @param string $message
     * @param array $errors
     * @return JsonResponse
     */
    public function respondBadRequest($message = '', $errors = [])
    {
        $message = (isset($message) && $message) ? $message : trans('api.error_bad_request');
        return $this->respondWithError($message, $errors, 400);
    }

    /**
     * Response on not found
     *
     * @param $message
     * @return JsonResponse
     */
    public function respondNotFound($message = '')
    {
        $message = (isset($message) && $message) ? $message : trans('api.error_not_found');
        return $this->respondWithError($message, [], 404);
    }

    /**
     * Response on success create
     *
     * @param $message
     * @param $new_instance
     * @return JsonResponse
     */
    public function respondCreated($message, $new_instance)
    {
        return $this->respondWithSuccess($message, $new_instance, 201);
    }

    /**
     * Response on successful update
     *
     * @param $message
     * @param $instance
     * @return JsonResponse
     */
    public function respondUpdated($message, $instance)
    {
        return $this->respondWithSuccess($message, $instance, 202);
    }

    /**
     * Response on successful delete
     *
     * @param $message
     * @return JsonResponse
     */
    public function respondDeleted($message = '')
    {
        $message = (isset($message) && $message) ? $message : trans('api.deleted_success');
        return $this->respondWithSuccess($message, [], 202);
    }

    /**
     * Response on success get
     *
     * @param $message
     * @param array $data
     * @return JsonResponse
     */
    public function respondSuccess($message = '', $data = [])
    {
        $message = (isset($message) && $message) ? $message : trans('api.success');
        return $this->respondWithSuccess($message, $data, 200);
    }

    /**
     * Validation
     *
     * @param Request $request
     * @param $rules
     * @throws ApiModelValidationException
     */
    protected function isValid(Request $request, $rules)
    {
        try {
            $this->validate($request, $rules);
        } catch (ValidationException $ex) {
            throw new ApiModelValidationException($ex);
        }
    }

}
