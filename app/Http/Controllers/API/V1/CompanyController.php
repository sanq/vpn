<?php

namespace App\Http\Controllers\API\V1;

use App\Exceptions\API\ApiModelNotFountException;
use App\Exceptions\API\ApiUnknownErrorException;
use App\Http\Controllers\API\BaseApiController;
use App\Models\Company\Company;
use App\Models\Company\CompanyTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CompanyController extends BaseApiController
{
    /**
     * Display a listing of the companies.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $companies = Company::all();
        return $this->respondSuccess(trans('api.success'), $this->createCollectionData($companies, new CompanyTransformer()));
    }

    /**
     * Store a newly created user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiUnknownErrorException
     */
    public function store(Request $request)
    {
        $this->isValid($request, Company::getValidationRules());

        $company = new Company([
            'name' => $request->input('name'),
            'quota' => $request->input('quota', 0)
        ]);

        if (!$company->save()) {
            throw new ApiUnknownErrorException();
        };

        return $this->respondCreated('', $this->createItemData($company, new CompanyTransformer()));
    }

    /**
     * Display the specified company.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiModelNotFountException
     */
    public function show($id)
    {
        $company = Company::find($id);
        if (!isset($company)) {
            throw new ApiModelNotFountException(trans('api.error_company_not_found'));
        }
        return $this->respondSuccess(trans('api.success'), $this->createItemData($company, new CompanyTransformer()));
    }

    /**
     * Update the specified company.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiUnknownErrorException|ApiModelNotFountException
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);

        if (!isset($company)) {
            throw new ApiModelNotFountException(trans('api.error_company_not_found'));
        }

        $this->isValid($request, Company::getValidationRules());

        $company->name = $request->input('name');
        $company->quota = $request->input('quota', 0);

        if (!$company->save()) {
            throw new ApiUnknownErrorException();
        };
        return $this->respondUpdated('', $this->createItemData($company, new CompanyTransformer()));
    }

    /**
     * Remove the specified company.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiModelNotFountException|ApiUnknownErrorException
     */
    public function destroy($id)
    {
        $company = Company::find($id);

        if (!isset($company)) {
            throw new ApiModelNotFountException(trans('api.error_company_not_found'));
        }

        if (!$company->delete()) {
            throw new ApiUnknownErrorException();
        };

        return $this->respondDeleted();
    }

    /**
     * Display the companies that exceeded their quota.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function blocked($month)
    {
        $month = (int)$month;
        $configMonth = config('my.report_last_months');
        if ($month < 1 || $month > $configMonth) {
            return $this->respondInoperableEntity(trans('validation.between.numeric', ['attribute' => trans('api.month'), 'min' => 1, 'max' => $configMonth]));
        }

        $companies = Collection::make(null);
        foreach (Company::all() as $company) {
            $company->calculateUsedBytes($month);
            if ($company->isExceededQuota()) {
                $companies->push($company);
            }
        }
        $companies->sortByDesc('bytes');
        return $this->respondSuccess(trans('api.success'), $this->createCollectionData($companies, new CompanyTransformer()));
    }

}
