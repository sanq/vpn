<?php

namespace App\Http\Controllers\API\V1;

use App\Exceptions\API\ApiModelNotFountException;
use App\Exceptions\API\ApiUnknownErrorException;
use App\Http\Controllers\API\BaseApiController;
use App\Models\Company\Company;
use App\Models\User\User;
use App\Models\User\UserTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class UserController extends BaseApiController
{
    /**
     * Display a listing of the users.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $users = User::all();
        return $this->respondSuccess(trans('api.success'), $this->createCollectionData($users, new UserTransformer()));
    }

    /**
     * Store a newly created user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiUnknownErrorException|ApiModelNotFountException
     */
    public function store(Request $request)
    {
        $this->isValid($request, User::getValidationRules());

        $company = Company::find($request->input('company_id'));
        if (!isset($company)) {
            throw new ApiModelNotFountException(trans('api.error_company_not_found'));
        }

        $user = new User([
            'company_id' => $company->id,
            'email' => $request->input('email'),
            'name' => $request->input('name'),
        ]);

        if (!$user->save()) {
            throw new ApiUnknownErrorException();
        };

        return $this->respondCreated('', $this->createItemData($user, new UserTransformer()));
    }

    /**
     * Display the specified user.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiModelNotFountException
     */
    public function show($id)
    {
        $user = User::find($id);
        if (!isset($user)) {
            throw new ApiModelNotFountException(trans('api.error_user_not_found'));
        }
        return $this->respondSuccess(trans('api.success'), $this->createItemData($user, new UserTransformer()));
    }

    /**
     * Update the specified user.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiUnknownErrorException|ApiModelNotFountException
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if (!isset($user)) {
            throw new ApiModelNotFountException(trans('api.error_user_not_found'));
        }

        $this->isValid($request, User::getValidationRules($user->id));

        $company = Company::find($request->input('company_id'));
        if (!isset($company)) {
            throw new ApiModelNotFountException(trans('api.error_company_not_found'));
        }

        $user->company_id = $company->id;
        $user->email = $request->input('email');
        $user->name = $request->input('name');

        if (!$user->save()) {
            throw new ApiUnknownErrorException();
        };
        return $this->respondUpdated('', $this->createItemData($user, new UserTransformer()));
    }

    /**
     * Remove the specified user.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiModelNotFountException|ApiUnknownErrorException
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if (!isset($user)) {
            throw new ApiModelNotFountException(trans('api.error_user_not_found'));
        }

        if (!$user->delete()) {
            throw new ApiUnknownErrorException();
        };

        return $this->respondDeleted();
    }

    /**
     * Display a listing of the abusers.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function abuser($month)
    {
        $month = (int)$month;
        $configMonth = config('my.report_last_months');
        if ($month < 1 || $month > $configMonth) {
            return $this->respondInoperableEntity(trans('validation.between.numeric', ['attribute' => trans('api.month'), 'min' => 1, 'max' => $configMonth]));
        }
        $users = Collection::make(null);
        foreach (Company::all() as $company) {
            $company->calculateUsedBytes($month);
            if ($company->isExceededQuota()) {
                $user = $company->users->sortbyDesc('bytes')->first();
                if (isset($user)) {
                    $users->push($user);
                }
            }
        }
        return $this->respondSuccess(trans('api.success'), $this->createCollectionData($users, new UserTransformer()));
    }

}
