<?php

namespace App\Http\Controllers\API\V1;

use App\Exceptions\API\ApiModelNotFountException;
use App\Http\Controllers\API\BaseApiController;
use App\Models\Company\Company;
use App\Models\Transfer\Transfer;
use App\Models\Transfer\TransferTransformer;
use App\Models\User\User;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TransferController extends BaseApiController
{
    /**
     * Display a listing of the transfers.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $transfers = Transfer::all();
        return $this->respondSuccess(trans('api.success'), $this->createCollectionData($transfers, new TransferTransformer()));
    }

    /**
     * Display the specified transfer.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiModelNotFountException
     */
    public function show($id)
    {
        $transfer = Transfer::find($id);
        if (!isset($transfer)) {
            throw new ApiModelNotFountException(trans('api.error_transfer_not_found'));
        }
        return $this->respondSuccess(trans('api.success'), $this->createItemData($transfer, new TransferTransformer()));
    }

    /**
     * Generate data
     *
     * @param \Faker\Generator $faker
     * @return \Illuminate\Http\JsonResponse
     */
    public function generate(\Faker\Generator $faker)
    {

        /**
         * Get last N months option
         */
        $monthNumber = config('my.report_last_months');

        /**
         * Clear transfers
         */
        Transfer::getQuery()->delete();

        Company::all()->each(function (Company $company) use ($faker, $monthNumber) {
            $company->users()->each(function (User $user) use ($faker, $monthNumber) {

                /**
                 *  Each user of the company has at least one transfer every month.
                 */
                $starFrom = Carbon::now()->endOfMonth()->addMinute()->subMonths($monthNumber);
                while ($starFrom < Carbon::now()) {
                    $user->transfers()->save(new Transfer([

                        /**
                         *  10995116277760 Bytes == 10TB
                         */
                        'transferred' => $faker->randomFloat(0, 100, 10995116277760),
                        'resource' => $faker->url,
                        'processed_at' => $starFrom,
                    ]));
                    $starFrom->addMonth();
                }

                /**
                 * General dummy data
                 */
                $user->transfers()->saveMany(factory(Transfer::class, rand(50, 500))->make());
            });
        });

        return $this->respondSuccess(trans('api.success'));
    }

}
