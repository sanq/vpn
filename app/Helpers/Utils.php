<?php

namespace App\Helpers;

class Utils
{

    public static function formatBytes($size, $precision = 2)
    {
        if ($size) {
            $base = log($size, 1024);
            $suffixes = array('B', 'K', 'M', 'G', 'T');
            return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[floor($base)];
        } else {
            return '0 B';
        }
    }
}