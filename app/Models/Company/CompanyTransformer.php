<?php

namespace App\Models\Company;

use App\Helpers\Utils;
use League\Fractal\TransformerAbstract;

class CompanyTransformer extends TransformerAbstract
{
    public function transform(Company $company)
    {
        $res = [
            'id' => (int)$company->id,
            'name' => $company->name,
            'quota' => Utils::formatBytes($company->quota),
            'bytes' => Utils::formatBytes($company->bytes),
            'quota_abs' => $company->quota,
        ];
        return $res;
    }

}
