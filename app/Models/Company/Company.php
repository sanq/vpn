<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * App\Models\Company\Company
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\User[] $users
 * @mixin \Eloquent
 */
class Company extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'quota'
    ];

    /**
     * Validation rules
     *
     * @return array
     */
    public static function getValidationRules()
    {
        return [
            'name' => 'required|max:150',
            'quota' => 'digits_between:0,15',
        ];
    }

    /**
     * Used bytes
     *
     * @var double
     */
    public $bytes = 0;

    /**
     * Quota mutator
     *
     * @param $value
     */
    public function setQuotaAttribute($value)
    {
        $this->attributes['quota'] = isset($value) ? $value : 0;
    }

    /**
     * Users
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\Models\User\User');
    }

    /**
     * Calculate used bytes
     *
     * @param int $month - months offset
     * @return double
     */
    public function calculateUsedBytes($month)
    {
        $month = Carbon::now()->subMonth($month - 1);
        $this->bytes = 0;
        foreach ($this->users as $user) {
            $user->bytes = $user->transfers()->byMonth($month)->sum('transferred');
            $this->bytes += $user->bytes;
        }
    }

    /**
     * Has the company exceed its quota
     *
     * @return bool
     */
    public function isExceededQuota()
    {
        return ($this->bytes > $this->quota);
    }

}
