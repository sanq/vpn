<?php
namespace App\Models\User;

use App\Helpers\Utils;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        $res = [
            'id' => (int)$user->id,
            'company_id' => $user->company_id,
            'email' => $user->email,
            'name' => $user->name,
            'bytes' => Utils::formatBytes($user->bytes),
        ];
        return $res;
    }

}
