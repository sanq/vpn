<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\User
 *
 * @property-read \App\Models\Company\Company $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transfer\Transfer[] $transfers
 * @mixin \Eloquent
 */
class User extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'name', 'email'
    ];

    /**
     * Validation rules
     *
     * @param null $id
     * @return array
     */
    public static function getValidationRules($id = null)
    {
        return [
            'company_id' => 'required|integer',
            'email' => 'required|email|unique:users,email,' . (isset($id) ? $id : 'NULL') . ',id|max:150',
            'name' => 'required|max:150',
        ];
    }

    /**
     * Used bytes
     *
     * @var double
     */
    public $bytes = 0;

    public function company()
    {
        return $this->belongsTo('App\Models\Company\Company');
    }

    public function transfers()
    {
        return $this->hasMany('App\Models\Transfer\Transfer');
    }

}
