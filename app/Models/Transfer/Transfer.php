<?php

namespace App\Models\Transfer;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * App\Models\Transfer\Transfer
 *
 * @property-read \App\Models\User\User $user
 * @mixin \Eloquent
 */
class Transfer extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transfers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'processed_at', 'resource', 'transferred'
    ];

    /**
     * Validation rules
     *
     * @return array
     */
    public static function getValidationRules()
    {
        return [
            'user_id' => 'required|integer',
            'resource' => 'required',
            'transferred' => 'required|numeric|max:15',
        ];
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User\User');
    }

    /**
     * Get transfers by specified month.
     *
     * @param $query
     * @param Carbon $month
     * @return mixed
     */
    public function scopeByMonth($query, Carbon $month)
    {
        $month->startOfMonth();
        $monthEnd = $month->copy()->endOfMonth();
        return $query->whereBetween('processed_at', [$month, $monthEnd]);
    }

}
