<?php

namespace App\Models\Transfer;

use App\Helpers\Utils;
use League\Fractal\TransformerAbstract;

class TransferTransformer extends TransformerAbstract
{
    public function transform(Transfer $transfer)
    {
        $res = [
            'id' => (int)$transfer->id,
            'user_id' => (int)$transfer->user_id,
            'processed_at' => $transfer->processed_at->timestamp,
            'resource' => $transfer->resource,
            'transferred' => Utils::formatBytes($transfer->transferred),
        ];
        return $res;
    }

}

