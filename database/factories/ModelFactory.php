<?php
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Company\Company::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'quota' => $faker->randomFloat(0, 100, 10995116277760),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
    ];
});

/**
 * Get start date
 */
$monthNumber = config('my.report_last_months');
$startDate = Carbon\Carbon::now()->endOfMonth()->subMonths($monthNumber);
$now = Carbon\Carbon::now();

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Transfer\Transfer::class, function (Faker\Generator $faker) use ($startDate, $monthNumber, $now) {

    $processed_at = $startDate->copy();
    $processed_at->addMonth(rand(1, $monthNumber - 1));
    if ($now->month == $processed_at->month) {
        $processed_at->addDay(rand(1, $now->day - 1));
    } else {
        $processed_at->addDay(rand(1, 28));
    }

    return [
        'resource' => $faker->url,
        /**
         *  10995116277760 Bytes == 10TB
         */
        'transferred' => $faker->randomFloat(0, 100, 10995116277760),
        'processed_at' => $processed_at,
    ];
});
