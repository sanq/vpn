<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(App\Models\Company\Company::class, 2)->create()->each(function ($company) {
            $company->users()->saveMany(factory(App\Models\User\User::class, 2)->make());

            /**
             * Transfer seed
             */
//            $company->users()->each(function ($user) {
//                $user->transfers()->saveMany(factory(App\Models\Transfer\Transfer::class, 10)->make());
//            });
        });

        Model::reguard();
    }
}
